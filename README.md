## 项目目录

> 1、separate-project-cors:前后端分离项目，后端处理跨域
>
> 2、separate-project-proxy:前后端分离项目，代理处理跨域
>
>     --开发环境可直接在前端配置代理
>     --生产环境下需要单独部署nginx等代理
>
> 3、single-backend:后端项目

## cas单点登录时序图

<img src="assets/cas_flow_diagram.png" alt="cas_flow_diagram.png" style="zoom: 80%;" />

## 前后端分离项目时序图

<img src="assets/separate.drawio.png"  />
