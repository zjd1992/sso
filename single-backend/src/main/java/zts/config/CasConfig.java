package zts.config;


import org.jasig.cas.client.boot.configuration.CasClientConfigurer;
import org.jasig.cas.client.boot.configuration.EnableCasClient;
import org.jasig.cas.client.configuration.ConfigurationKeys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.util.Map;


@Configuration
@EnableCasClient
public class CasConfig implements CasClientConfigurer, ConfigurationKeys {
    @Value("${cas-ignore-pattern:}")
    private String casIgnorePattern;

    @Override
    public void configureAuthenticationFilter(FilterRegistrationBean authenticationFilter) {
        Map<String,String> initParameters = authenticationFilter.getInitParameters();
        if (StringUtils.hasText(casIgnorePattern)) {
            initParameters.put(IGNORE_PATTERN.getName(), casIgnorePattern);
        }
    }
}

