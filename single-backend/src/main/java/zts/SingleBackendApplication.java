package zts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SingleBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SingleBackendApplication.class, args);
    }

}
