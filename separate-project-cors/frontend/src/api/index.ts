import axios from 'axios'
import { casLogin } from '../utils/cas'

let request = axios.create({
  baseURL: import.meta.env.VITE_BACK_URL,
  withCredentials: true,
})


request.interceptors.response.use(
  response => {
    switch (response.status) {
      case 200:
        return response.data
      case 203 /* 处理cas 重定向 */:
        casLogin()
        break
      default:
        return Promise.reject()
    }
  }
)

export default request
