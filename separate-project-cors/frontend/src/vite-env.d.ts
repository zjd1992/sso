/// <reference types="vite/client" />
interface ImportMetaEnv {
  readonly VITE_BACK_URL: string
  readonly VITE_LOGOUT_URL: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
