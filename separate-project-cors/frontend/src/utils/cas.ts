export const casLogin = () => {
  let url = encodeURIComponent(window.location.href) /* 登录后跳转到原页面 */
  window.location.href = `${import.meta.env.VITE_BACK_URL}/login?url=${url}`
}

export const casLogout = () => {
  let url = encodeURIComponent(import.meta.env.VITE_LOGOUT_URL) /* 重新登录后跳转到主页 */
  window.location.href = `${import.meta.env.VITE_BACK_URL}/logout?url=${url}`
}
