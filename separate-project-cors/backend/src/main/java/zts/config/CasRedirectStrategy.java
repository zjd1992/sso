package zts.config;

import org.jasig.cas.client.authentication.AuthenticationRedirectStrategy;
import org.jasig.cas.client.util.CommonUtils;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CasRedirectStrategy implements AuthenticationRedirectStrategy {

    @Override
    public void redirect(HttpServletRequest request, HttpServletResponse response, String potentialRedirectUrl) throws IOException {
        /* 通过Origin判断前后端分离项目跨域请求 */
        if (CommonUtils.isNotBlank(request.getHeader("Origin"))){
            /* 跨域处理：cas过滤器优先级高，自定义跨域配置无法处理此请求 */
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials","true");
            /* 自定义状态码,根据实际情况自定义，前端需对应此值 */
            response.setStatus(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
        }else {
            response.sendRedirect(potentialRedirectUrl);
        }
    }
}

