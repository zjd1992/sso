import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  let env = loadEnv(mode, process.cwd(), '')

  return {
    plugins: [vue()],
    base: './',
    server: {
      host: env.VITE_HOST,
      port: ~~env.VITE_PORT,
      proxy: {
        [env.VITE_BASE_API]: {
          target: env.VITE_BACK_URL,
          changeOrigin: true,
          rewrite: path => path.replace(RegExp(`^${env.VITE_BASE_API}`), ''),
        },
      },
    },
  }
})
