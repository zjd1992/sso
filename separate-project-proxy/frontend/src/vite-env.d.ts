/// <reference types="vite/client" />
interface ImportMetaEnv {
  readonly VITE_HOST: string
  readonly VITE_PORT: number
  readonly VITE_BASE_API: string
  readonly VITE_BACK_URL: string
  readonly VITE_LOGOUT_URL: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
