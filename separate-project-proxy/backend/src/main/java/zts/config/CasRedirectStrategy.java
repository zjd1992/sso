package zts.config;

import org.jasig.cas.client.authentication.AuthenticationRedirectStrategy;
import org.jasig.cas.client.util.CommonUtils;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CasRedirectStrategy implements AuthenticationRedirectStrategy {

    @Override
    public void redirect(HttpServletRequest request, HttpServletResponse response, String potentialRedirectUrl) throws IOException {
        if (CommonUtils.isNotBlank(request.getHeader("Cas-Flag"))) {
            response.setStatus(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
        } else {
            response.sendRedirect(potentialRedirectUrl);
        }
    }
}

